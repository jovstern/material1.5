# Taykey Angular 1.5 seed

A complete, yet simple, starter for Angular using Webpack.

>Warning: Make sure you're using the latest version of Node.js and NPM

### Quick start

> Clone/Download the repo then edit `app.js` inside [`/app/src/app.js`](/app/src/app.js)

```bash
# clone our repo
$ git clone https://bitbucket.org/taykey/webapps.angular-taykey-seed

# change directory to your app
$ cd my-app

# install the dependencies with npm
$ npm install

# start the server
$ npm start
```

go to [http://localhost:8080](http://localhost:8080) in your browser.

It will start a local server using `webpack-dev-server` which will watch, build (in-memory), and reload for you. The port will be displayed to you as `http://localhost:8080`.

## Developing

### Build files

* single run: `npm run build`


# License

[MIT](/LICENSE)
