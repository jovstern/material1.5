'use strict';

window.moment = require('moment-timezone');

import angular from 'angular';
import uirouter from 'angular-ui-router';
import angularAria from 'angular-aria';
import angularAnimate from 'angular-animate';
import ngMaterial from 'angular-material';
import ngMdIcons from 'material-design-icons';

import home from './components/home/home';
import navbar from './components/navbar/navbar';
import timeZone from './constants/time-zone';

// Taykey Dependencies
import 'tk-utils';
import 'audience-service';
import 'entity-service';
import 'angular-sanitize';
import 'tk-common';


// Styles import
import '../style/app.scss';

class AppCtrl {
    /*@ngInject*/
    constructor() {
        this.selectedItem = null;
    }
}

let appComponent = {
    template: require('./app.html'),
    controller: AppCtrl,
    controllerAs: 'app'
};

export default angular.module('app', [
    angularAria,
    angularAnimate,
    ngMaterial,
    home,
    navbar,
    'audience-service',
    'tk-common',
    'entity-service'
]).config(function($mdIconProvider, $mdThemingProvider) {
    $mdIconProvider
        .iconSet('social', 'img/icons/social-icons.svg', 24)
        .defaultIconSet('img/icons/core-icons.svg', 24);
    $mdThemingProvider.theme('default')
        .primaryPalette('pink')
        .accentPalette('orange');
}).component('app', appComponent).name;

