import angular from 'angular';
import sampleService from '../../services/sample.service';
import template from './home.html';

// Taykey dependencies
import 'user-service';


class HomeCtrl {
    /*@ngInject*/
    constructor(userService, sampleService) {
        this.url = 'http://taykey.com/';
        this.userService = userService;
        this.sampleService = sampleService;
    }

    $onInit() {
        this.items = this.sampleService.getItems();

        this.userService.me().then(res => {
            this.user = res.firstName + " " + res.lastName;
        })
    }
}

let options =  {
    bindings: {

    },
    controllerAs: "$home",
    controller: HomeCtrl,
    template
};

export default angular.module("app.home", [sampleService, 'user-service'])
    .component('home', options).name;