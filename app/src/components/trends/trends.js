import template from './trends.html';


class TrendsCtrl{
    constructor(){
        this.items = [
            {name: "Yael"},
            {name: "Yoav"},
            {name: "Nir"},
            {name: "Amit"},
            {name: "Refael"},
        ];
    }
    $onChanges(changes){

        if (changes.selectedItem.currentValue){
            this.selectedWorker = this.items[changes.selectedItem.currentValue];
        }

    }
}

 let options = {
     bindings:{
         selectedItem:'<'
     },
     controller: TrendsCtrl,
     controllerAs: "$trends",
     template
 };

export default angular.module("app.trends", [])
    .component('trends', options)
    .name;