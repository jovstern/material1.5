import angular from 'angular';

const timeZone = 'America/New_York';

export default angular.module('constants.time-zone', [])
    .constant('TIME_ZONE', 'America/New_York')
    .name;


