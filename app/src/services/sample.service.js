import angular from 'angular';

class SampleCtrl {
    /*@ngInject*/
    constructor() {
        this.items = [{ name: 'Amit' }, { name: 'Nir' }, { name: 'Yael' }];
    }

    getItems() {
        return this.items;
    }
}

export default angular.module('services.sample', [])
    .service('sampleService', SampleCtrl)
    .name;